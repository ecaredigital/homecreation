<?php

use Illuminate\Support\Facades\Route;

Route::get("/", "Front\PageController@getIndex");
Route::get("/home", "HomeController@index");


//******************************************User*****************************************

Route::middleware("auth")->prefix("/inventory")->group(function () {
    Route::resource("/category", "Inventory\CategoryController");
    Route::resource("/subcategory", "Inventory\SubCategoryController");

    Route::get("/order-request", "ProductOrderController@index")->name("order.index");
    Route::get("/product-order/{id}", "ProductOrderController@show")->name("order.show");
    Route::delete("/order/{id}", "ProductOrderController@destroy")->name("order.delete");
    Route::resource("/product", "Inventory\ProductController", ['except'=>['show']]);
    Route::get("product/newly_arrival", "Inventory\ProductController@newly_arrival")->name("product.newly_arrival");
    Route::get("product/special_offer", "Inventory\ProductController@special_offer")->name("product.special_offer");

    Route::resource('/size', "Inventory\SizeController", ['except'=>['create','edit','show']]);

    Route::get("/stock-history", "Inventory\StockController@history")->name("stock.history");
    Route::put("/stock-update/{id}", "Inventory\StockController@insert")->name("stock.insert");
    Route::resource("/stock", "Inventory\StockController");

    Route::get("/transaction-cancel/{id}", "Inventory\TransactionController@cancel")->name("transaction.cancel");

    Route::get("/transaction/product", "Inventory\ProductTransactionController@index")->name("product.transaction.index");
    Route::get("/transaction/product-search", "Inventory\ProductTransactionController@search");
    Route::get("/transaction/product/{id}", "Inventory\ProductTransactionController@show");

    Route::get("/transaction/report", "Inventory\TransactionController@history")->name("transaction.report");
    Route::get("/sell-history", "Inventory\TransactionController@index")->name("transaction.index");
    Route::resource("/transaction", "Inventory\TransactionController", ['except'=>['show','edit','index']]);


    Route::get("/user-transaction", "Inventory\UserTransactionController@index")->name("user.transaction.index");
    Route::post("/user-transaction", "Inventory\UserTransactionController@store");
    Route::get("/user-transaction/{user}", "Inventory\UserTransactionController@show");

    Route::resource("/quality", "Inventory\QualityController");
});
//******************************************User*****************************************

//******************************************Admin start*****************************************

Route::middleware("auth")->prefix("/admin")->as("admin.")->group(function () {
    Route::resource("/slider", "Front\SliderController", [
       "except"=>[
           'show'
       ]
   ]);

    Route::resource("/worker", "LeaderShipController", [
       'except'=>'show'
   ]);


    Route::resource("/feedback", "Front\FeedbackController");
    Route::resource("/event", "EventController");
    Route::resource("/latest_activities", "LatestActivitiesController");
    Route::resource("/production_units", "ProductionUnitsController");
    Route::resource("/business", "BusinessController");
    Route::resource("/business_category", "BusinessCategoryController");
    Route::resource("/event", "EventController");
    Route::resource("/gallery", "ImageUploadController");
//    Route::get('gallery','ImageUploadController@index');
    Route::get('gallery/create','ImageUploadController@fileCreate');
    Route::post('gallery/store','ImageUploadController@fileStore');
    Route::post('gallery/delete','ImageUploadController@fileDestroy');

    Route::get("/about-us", "HomeController@getAboutAdmin")->name("about");
    Route::post("/about-overview", "HomeController@storeAboutOverview")->name("about.overview");
    Route::get("/core-value", "HomeController@getCoreValue")->name("core");
    Route::post("/core-value", "HomeController@storeCoreValue")->name("core.store");
    Route::get("/contact", "ContactController@index")->name("contact.index");
    Route::get("/message/{id}", "ContactController@show");
    Route::delete("/message/{id}", "ContactController@destroy");

    Route::get("/company-detail", "CompanyDetailController@index")->name("company.detail");
    Route::post("/company-detail", "CompanyDetailController@store")->name("company.detail.store");

    Route::get("/send-mail","MailController@create");
    Route::post("/send-mail","MailController@store");

    Route::get("/mission", "HomeController@getMission")->name("mission");
    Route::post("/mission", "HomeController@storeMission")->name("mission.store");

    Route::get("/vision", "HomeController@getVision")->name("vision");
    Route::post("/vision", "HomeController@storeVision")->name("vision.store");

    Route::get("/objective", "HomeController@getObjective")->name("objective");
    Route::post("/objective", "HomeController@storeObjective")->name("objective.store");
});
//******************************************Admin End*****************************************


//******************************************Public*****************************************

Route::get("/about", "Front\PageController@getAbout");

Route::get("/products", "Front\ProductController@index");
Route::get("/product/{id}", "Front\ProductController@show")->name("public.product.show");
Route::get("/newly_arrived", "Front\ProductController@newly_arrival");
Route::get("/special_offer", "Front\ProductController@special_offer");
Route::get("/testimonial", "Front\PageController@getTestimonial");

Route::get("/vision", "Front\PageController@getvision");
Route::get("/mission", "Front\PageController@getMission");
Route::get("/objective", "Front\PageController@getobjective");
Route::get("/production_units", "Front\PageController@getProductionUnit");
Route::get("/latest_activity", "Front\PageController@getLatestActivities");
Route::get("/upcoming_event", "Front\PageController@getEvents");

Route::get("/search", "Front\ProductController@search");
Route::get("/categories", "Front\CategoryController@index");
Route::get("/subcategory/{id}", "Front\PageController@getProductsBySubCategory");

Route::post("/product-order", "ProductOrderController@store")->name("order.store");

Route::resource("/user", "UserController")->middleware("auth");

Route::get("/contact", "Front\PageController@getContact");
Route::post("/contact", "Front\PageController@storeContact");

Route::get("/login","Auth\LoginController@showLoginForm")->name("login");
Route::post("/login","Auth\LoginController@login");
Route::post("/logout","Auth\LoginController@logout")->name("logout");
Route::get("/StakeHolder","Front\PageController@getStakeHolder");
Route::get("/Stake/{id}","Front\PageController@getStake");
Route::get("/DealWithPatner","Front\PageController@getDealWithPatner");
Route::get("/DealWithPatnerid/{id}","Front\PageController@getDealWithPatnerId");
Route::get("/gallery","Front\PageController@getGallery");


//******************************************Public end*****************************************
/* **************************desigm part starts Here ************************************* */


Route::prefix("/design")->group(function() {


    Route::get("/check", function () {
        return view("public.pages.contact");
    });


    Route::get("/newly", function () {
        return view("design.newlyarrive");
    });
    Route::get("/category", function () {
        return view("design.category");
    });
    Route::get("/newly", function () {
        return view("design.newlyarrive");
    });
    Route::get("/vision", "Front\PageController@getvision");
    Route::get("/mission", "Front\PageController@getMission");
    Route::get("/objective", "Front\PageController@getobjective");
//    Route::get("/mission", function () {
//        return view("design.mission");
//    });

    Route::get("/upcoming_event", function () {
        return view("design.upcoming_event");
    });

    Route::get("/unit", function () {
        return view("design.unit");
    });
    Route::get("/activity", function () {
        return view("design.latest_activity");
    });
    Route::get("/gallery", function () {
        return view("design.gallery");
    });
    Route::get("/special", function () {
        return view("design.special_offer");
    });


    Route::get("/addevent", function () {
        return view("design.addevents");
    });
    Route::get("/feedback", function () {
        return view("design.customer_feedback");
    });
    Route::get("/stakeholder", function () {
        return view("design.stake_holder");
    });
    Route::get("/deal", function () {
        return view("design.deal_partner");
    });




});
