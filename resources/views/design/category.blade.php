@extends("layouts.public")
@section("contact")
    <link href="{{ asset('css/category.css') }}" rel="stylesheet" >
<div class="container">
    <div class="row">
        <div class="header">
            <h4>Global Adventure & Mountaineering Conference and Expo (GAMCE 2018)</h4>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                </div>
                <div class="row detail">
                    <span class="date"><i class="fa fa-calendar"></i> Wed, 21 - Sat, 24 Nov 2018</span>
                    <span class="time"><i class="fa fa-clock-o"></i> 1:00 AM</span>
                    <span class="hotel"><i class="fa fa-map-marker"></i> Hotel Yak and Yeti, Kathmandu</span><br>
                    <span class="details">Organized by:<a href="#"> Home Creation</a> </span><br>
                    <p>Global Adventure & Mountaineering Conference and Expo is a platform to engage with the outdoor
                        adventure industry whether you are enhancing your profession or networking partnership, whether gathering
                        knowledge or lobbying for a cause or you may be simply buying or selling.<br>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                        industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and
                        scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap
                        into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the
                        release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing
                        software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                </div>
                <div class="row footer">

                </div>
            </div>
        </div>
    </div>
</div>


@endsection