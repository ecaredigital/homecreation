@extends("layouts.public")
<link href="{{ asset('css/sub_heading.css') }}" rel="stylesheet">
<link href="{{ asset('css/customer_feedback.css') }}" rel="stylesheet">


@section("contact")
    <div class="sub_heading_title">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-12">
                    <h1 class="h1 text-center">
                        <b>Customer Feedback</b></h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="paragraph">
           <div class="row testimonial">
               <div class="col-md-3">
                   <div class="box_shadow card">
                     <i><p><i class="fa fa-quote-left fa-2x testimonial_icon"></i>
                             A company where a girl can find the a fashionable outfit for any occasion and while they are priced high, the designs are very worth while.A company where a girl can find the a fashionable outfit for any occasion and while they are priced high, the designs are very worth while.<i class="fa fa-quote-right fa-2x testimonial_icon"></i></p>
                           <div class="contact">
                                <span class="user-name">Shiva Dulal</span><br>
                               <span class="grey">Ecare Digital Pvt Ltd</span>
                           </div>
                     </i>
                   </div>
               </div>
               <div class="col-md-3">
                   <div class="box_shadow card">
                     <i><p><i class="fa fa-quote-left fa-2x testimonial_icon"></i>
                             Well I have been shopping online since 2012.  Before that I had lots of fear that something wud go wrong while buying online but that fear went off when I purchased my product through Flipkart for my mother in 2012!  It was b'day present for her and product was "a couple of books".<purchases class=""></purchases>
                           <i class="fa fa-quote-right fa-2x testimonial_icon"></i></p>
                           <div class="contact">
                                <span class="user-name">Milan Dulal</span><br>
                               <span class="grey">Ecare Digital Pvt Ltd</span>
                           </div>
                     </i>
                   </div>
               </div>
               <div class="col-md-3">
                   <div class="box_shadow card">
                       <i><p><i class="fa fa-quote-left fa-2x testimonial_icon"></i>
                               A company where a girl can find the a fashionable outfit for any occasion and while they are priced high, the designs are very worth while.<i class="fa fa-quote-right fa-2x testimonial_icon"></i></p>
                           <div class="contact">
                               <span class="user-name">Shiva Dulal</span><br>
                               <span class="grey">Ecare Digital Pvt Ltd</span>
                           </div>
                       </i>
                   </div>
               </div>
               <div class="col-md-3">
                   <div class="box_shadow card">
                       <i><p><i class="fa fa-quote-left fa-2x testimonial_icon"></i>
                               AliExpress is part of the Alibaba group of companies and is also featured on the Alibaba website. They were founded in 2009 as a marketplace that offers product soffers products at wholesale or factory prices. Most of their services are like Alibaba except for the minimum amount per order, manner of shipping and location of suppliers.<i class="fa fa-quote-right fa-2x testimonial_icon"></i></p>
                           <div class="contact">
                               <span class="user-name">Shiva Dulal</span><br>
                               <span class="grey">Ecare Digital Pvt Ltd</span>
                           </div>
                       </i>
                   </div>
               </div>
               <div class="col-md-3">
                   <div class="box_shadow card">
                       <i><p><i class="fa fa-quote-left fa-2x testimonial_icon"></i>
                               Carrying over 300 of the world’s leading brands and shipping to over 170 countries, Mr Porter has established itself as a premium online shopping site carrying everything a man could need. With offices in London and New York they are able to cater to able to cater to customers across the world and provide a world-class <service class=""></service><purchases class=""></purchases>
                               <i class="fa fa-quote-right fa-2x testimonial_icon"></i></p>
                           <div class="contact">
                               <span class="user-name">Milan Dulal</span><br>
                               <span class="grey">Ecare Digital Pvt Ltd</span>
                           </div>
                       </i>
                   </div>
               </div>

               <div class="col-md-3">
                   <div class="box_shadow card">
                       <i><p><i class="fa fa-quote-left fa-2x testimonial_icon"></i>
                               A company where a girl can find the a fashionable outfit for any occasion and whiled the a fashionable outfit for any occasion and while they are priced high, the designs are very worth while.<i class="fa fa-quote-right fa-2x testimonial_icon"></i></p>
                           <div class="contact">
                               <span class="user-name">Shiva Dulal</span><br>
                               <span class="grey">Ecare Digital Pvt Ltd</span>
                           </div>
                       </i>
                   </div>
               </div>

               <div class="col-md-3">
                   <div class="box_shadow card">
                       <i><p><i class="fa fa-quote-left fa-2x testimonial_icon"></i>
                               A company where a girl can find the a fashionable outfit for any occasion and while they are priced high, for any occasion and and while they are priced high, for any occasion and and while they are priced high, for any occasion and while they the designs are very worth while.<i class="fa fa-quote-right fa-2x testimonial_icon"></i></p>
                           <div class="contact">
                               <span class="user-name">Shiva Dulal</span><br>
                               <span class="grey">Ecare Digital Pvt Ltd</span>
                           </div>
                       </i>
                   </div>
               </div>
               <div class="col-md-3">
                   <div class="box_shadow card">
                       <i><p><i class="fa fa-quote-left fa-2x testimonial_icon"></i>
                               A company where a girl can find the a fashionable outfit for any occasion and while they are priced high, the designs are very worth while.<i class="fa fa-quote-right fa-2x testimonial_icon"></i></p>
                           <div class="contact">
                               <span class="user-name">Shiva Dulal</span><br>
                               <span class="grey">Ecare Digital Pvt Ltd</span>
                           </div>
                       </i>
                   </div>
               </div>


           </div>

        </div>
    </div>
    {{--modal for read more--}}
    <div class="modal fade" id="readmore" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Global Adventure & Mountaineering Conference and Expo (GAMCE 2018)</h4>
                </div>
                <div class="modal-body">
                    <span class="event_date"><i class="fa fa-calendar"></i>Wed, 21 - Sat, 24 Nov 2018</span>
                    <span class="event_date"><i class="fa fa-clock-o"></i>1:00 AM</span>
                    <span class="event_details"><i class="fa fa-map-marker"></i> Hotel Yak and Yeti, Kathmandu</span><br><br>
                    <span class="event_details">Organized by:<a href="#">Home Creation</a> </span><br>

                    <p >Global Adventure & Mountaineering Conference and Expo is a platform to engage with the outdoor adventure industry whether you are enhancing your profession or networking a partnership, whether gathering knowledge or lobbying for a cause or you may be simply buying or selling.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    {{--end of modal for see more--}}
@endsection
