@extends("layouts.public")
<link href="{{ asset('css/sub_heading.css') }}" rel="stylesheet">
<link href="{{ asset('css/upcoming_events.css') }}" rel="stylesheet">


@section("contact")
    <div class="sub_heading_title">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-12">
                    <h1 class="h1 text-center">
                        <b>Upcoming Events</b></h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="paragraph">
           <div class="row card upcoming_event">
                   <h3 class="event-title">Insitute Of Mountain Emergency Medicine World Congress (IMEMWC)</h3>
                   <span class="event_date"><i class="fa fa-calendar"></i>Wed, 21 - Sat, 24 Nov 2018</span>
                   <span class="event_date"><i class="fa fa-clock-o"></i>1:00 AM</span>
               <span class="event_details"><i class="fa fa-map-marker"></i> Hotel Yak and Yeti, Kathmandu</span><br>
               <span class="event_details">Organized by:<a href="#">Home Creation</a> </span><br>

               <p >The Insitute Of Mountain Emergency Medicine World Congress, organized by the EURAC research will take place from 21st November to 24th November 2018 at the Hotel Yak and Yeti in Kathmandu, Nepal. The conference will cover areas like Emergency Medicine.
                   <a href="#" data-toggle="modal" data-target="#readmore"> ...Read more</a></p>
               </p>
           </div>
            <div class="row card upcoming_event">
                <h3 class="event-title">Nepal Tattoo Convention</h3>
                <span class="event_date"><i class="fa fa-calendar"></i>Thursday, September 27, 2018</span>
                <span class="event_date"><i class="fa fa-clock-o"></i>1:00 AM</span>
                <span class="event_details"><i class="fa fa-map-marker"></i> Heritage Garden, Patan,  Nepal</span><br>
                <span class="event_details">Organized by:<a href="#">Home Creation</a> </span><br>

                <p >Nepal Tattoo Convention aims to promote art, artists and create a harmony amongst artists, collectors, enthusiasts and also with the critics. We are the first to organize a gathering of tattoo artists in the subcontinent and now we proud to invite, host and to showcase the ocean of tattoo artists, piercing and body mod artists and enthusiasts from all over the globe.
                    <a href="#" data-toggle="modal" data-target="#readmore"> ...Read more</a></p>
                </p>
            </div>
            <div class="row card upcoming_event">
                <h3 class="event-title">Premier Schools Exhibition </h3>
                <span class="event_date"><i class="fa fa-calendar"></i>Thu, 01 - Fri, 02 Nov 2018</span>
                <span class="event_date"><i class="fa fa-clock-o"></i>1:00 AM</span>
                <span class="event_details"><i class="fa fa-map-marker"></i> The British College, Kathmandu, Kathmandu</span><br>
                <span class="event_details">Organized by:<a href="#">Home Creation</a> </span><br>

                <p >"Asia’s leading & the oldest exhibition on School Admissions."
                    Premier Schools Exhibition - Kathmandu is an excellent platform to interact face-to-face with thousands of parents seeking admission for their child. Communicate directly with the parents about your school, its unique features, admission procedure, and cost. At the fair, conduct on the spot students enrolments & offer admission to the deserving candidates.
                    <a href="#" data-toggle="modal" data-target="#readmore"> ...Read more</a></p>
                </p>
            </div>

        </div>
    </div>
    {{--modal for read more--}}
    <div class="modal fade" id="readmore" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Global Adventure & Mountaineering Conference and Expo (GAMCE 2018)</h4>
                </div>
                <div class="modal-body">
                    <span class="event_date"><i class="fa fa-calendar"></i>Wed, 21 - Sat, 24 Nov 2018</span>
                    <span class="event_date"><i class="fa fa-clock-o"></i>1:00 AM</span>
                    <span class="event_details"><i class="fa fa-map-marker"></i> Hotel Yak and Yeti, Kathmandu</span><br><br>
                    <span class="event_details">Organized by:<a href="#">Home Creation</a> </span><br>

                    <p >Global Adventure & Mountaineering Conference and Expo is a platform to engage with the outdoor adventure industry whether you are enhancing your profession or networking a partnership, whether gathering knowledge or lobbying for a cause or you may be simply buying or selling.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    {{--end of modal for see more--}}
@endsection
