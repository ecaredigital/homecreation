@extends("layouts.public")
@section("contact")
    <link href="{{ asset('css/newly_arrive.css') }}" rel="stylesheet" >
    <div class="container-fluid demo-3">
        <div class="content">
            <div id="large-header" class="large-header">
                <canvas id="demo-canvas"></canvas>
                <h1 class="main-title"><span>Newly Arrive</span></h1>
            </div>
        </div>
    </div><!-- /container -->
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-3">
                <sidebar :sub_id="{!!request('sub',0)!!}" :sort="'{!!request('sort','latest')!!}'"></sidebar>
            </div>
                <div class="col-md-8 products">
                    <div class="row text-center">
                        <div class="col-md-3">
                            <div class="card">
                                <img src="{{ asset('images/products/15a71b673be6ff.png') }}" style="width:100%;">
                                <div class="offer">
                                    <img src="{{ asset('images/products/Untitled-2.gif') }}" style="width:56%;height: 20%;">
                                </div>
                                <div class="row details">
                                    <h4>Black Shoes </h4>
                                    <p>Leather Product <strong>|</strong> Shoes</p>
                                </div>
                                <button type="button" class="btn btn-success">View Product</button>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <img src="{{ asset('images/products/15a7cf5568f8ed.png') }}" style="width:100%;">
                                <div class="row details">
                                    <h4>Blue Diary</h4>
                                    <p>Paper Product <strong>|</strong> Diary</p>
                                </div>
                                <button type="button" class="btn btn-success">View Product</button>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <img src="{{ asset('images/products/15a7cfa6b69280.png') }}" style="width:100%;">
                                <div class="row details">
                                    <h4>Wood Window</h4>
                                    <p>Wood Product <strong>|</strong> Window</p>
                                </div>
                                <button type="button" class="btn btn-success">View Product</button>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <img src="{{ asset('images/products/15a71b673be6ff.png') }}" style="width:100%;">
                                <div class="row details">
                                    <div class="offer">
                                        <img src="{{ asset('images/products/Untitled-2.gif') }}" style="width:56%;height: 20%;">
                                    </div>
                                    <h4>Black Shoes </h4>
                                    <p>Leather Product <strong>|</strong> Shoes</p>
                                </div>
                                <button type="button" class="btn btn-success">View Product</button>
                            </div>
                        </div>
                     </div>
                </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-8 products">
                <div class="row text-center">
                    <div class="col-md-3">
                        <div class="card">
                            <img src="{{ asset('images/products/15a71b673be6ff.png') }}" style="width:100%;">
                            <div class="offer">
                                <img src="{{ asset('images/products/Untitled-2.gif') }}" style="width:56%;height: 20%;">
                            </div>
                            <div class="row details">
                                <h4>Black Shoes </h4>
                                <p>Leather Product <strong>|</strong> Shoes</p>
                            </div>
                            <button type="button" class="btn btn-success">View Product</button>
                        </div>
                    </div>
                    <div class="col-md-3 ">
                        <div class="card">
                            <img src="{{ asset('images/products/15a7cf5568f8ed.png') }}" style="width:100%;">
                            <div class="row details">
                                <h4>Blue Diary</h4>
                                <p>Paper Product <strong>|</strong> Diary</p>
                            </div>
                            <button type="button" class="btn btn-success">View Product</button>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card">
                            <img src="{{ asset('images/products/15a7cfa6b69280.png') }}" style="width:100%;">
                            <div class="row details">
                                <h4>Wood Window</h4>
                                <p>Wood Product <strong>|</strong> Window</p>
                            </div>
                            <button type="button" class="btn btn-success">View Product</button>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card">
                            <img src="{{ asset('images/products/15a7cfa6b69280.png') }}" style="width:100%;">
                            <div class="row details">
                                <h4>Window</h4>
                                <p>Wood Product <strong>|</strong> Window</p>
                            </div>
                            <button type="button" class="btn btn-success">View Product</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row text-center">
            <div class="col-md-3"></div>
            <div class="col-md-8 products">
                <div class="row">
                    <div class="col-md-3">
                        <div class="card">
                            <img src="{{ asset('images/products/15a71b673be6ff.png') }}" style="width:100%;">
                            <div class="row details">
                                <h4>Black Shoes </h4>
                                <p>Leather Product <strong>|</strong> Shoes</p>
                            </div>
                            <button type="button" class="btn btn-success">View Product</button>
                        </div>
                    </div>
                    <div class="col-md-3 ">
                        <div class="card">
                            <img src="{{ asset('images/products/15a7cf5568f8ed.png') }}" style="width:100%;">
                            <div class="row details">
                                <h4>Blue Diary</h4>
                                <p>Paper Product <strong>|</strong> Diary</p>
                            </div>
                            <button type="button" class="btn btn-success">View Product</button>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card">
                            <img src="{{ asset('images/products/15a7cfa6b69280.png') }}" style="width:100%;">
                            <div class="row details">
                                <h4>Wood Window</h4>
                                <p>Wood Product <strong>|</strong> Window</p>
                            </div>
                            <button type="button" class="btn btn-success">View Product</button>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card">
                            <img src="{{ asset('images/products/15a71b673be6ff.png') }}" style="width:100%;">
                            <div class="row details">
                                <h4>Black Shoes </h4>
                                <p>Leather Product <strong>|</strong> Shoes</p>
                            </div>
                            <button type="button" class="btn btn-success">View Product</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@section("script")
    <script src="{{ asset('js/TweenLite.min.js') }}"></script>
    <script src="{{ asset('js/EasePack.min.js') }}"></script>
    <script src="{{ asset('js/demo-3.js') }}"></script>

@endsection
@section('scripts')
    <script>
        new Vue({
            el:"#wrapper",
            data:{
                sub:{!!request('sub',0)!!},
                sort:'{!!request('sort','latest')!!}'
            },

            methods:{
                changeProduct:function(){
                    window.location.href="/design/newly?sub="+this.sub+"&sort="+this.sort;
                }
            },

        });

    </script>
@endsection
