@extends("layouts.public")
@section("contact")
    <link href="{{ asset('css/gallery.css') }}" rel="stylesheet" >
<div class="container-fluid">
    <div class="row text-center photo">
        <h4>Gallery</h4>
    </div>
</div>
                <div class="modal fade" role="dialog" id="modal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button class="close" data-dismiss="modal">X</button>
                                <h3>Home Creation Gallery</h3>
                            </div>
                            <div class="modal-body">
                                <img src="{{ asset('images/products/15a7c7005031ea.png') }}" style="width: 100%; height: 335px;">

                            </div>
                            <div class="modal-footer">
                                Gallery
                            </div>
                        </div>
                    </div>
                </div>

    <div class="container first_line">
        <div class="row">
            <div class="col-md-3" data-toggle="modal" data-target="#modal">
                <div class="panel panel-default first_panel">
                    <a href="#"><img src="{{ asset('images/products/15a7cf1459f36b.png') }}" style="width: 100%; height: 250px;"></a>
                </div>
            </div>
            <div class="col-md-3" data-toggle="modal" data-target="#modal">
                <div class="panel panel-default first_panel">
                    <a href="#"><img src="{{ asset('images/products/15a7cf5568f8ed.png') }}" style="width: 100%; height: 250px;"></a>
                </div>
            </div>
            <div class="col-md-3" data-toggle="modal" data-target="#modal">
                <div class="panel panel-default first_panel">
                    <a href="#"><img src="{{ asset('images/products/15a7cf47909b35.png') }}" style="width: 100%; height: 250px;"></a>
                </div>
            </div>
            <div class="col-md-3" data-toggle="modal" data-target="#modal">
                <div class="panel panel-default first_panel">
                    <a href="#"><img src="{{ asset('images/products/15a7cf5568f8ed.png') }}" style="width: 100%; height: 250px;"></a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-3" data-toggle="modal" data-target="#modal">
                <div class="panel panel-default first_panel">
                    <a href="#"><img src="{{ asset('images/products/15a7cf85370f8c.png') }}" style="width: 100%; height: 250px;"></a>
                </div>
            </div>
            <div class="col-md-3" data-toggle="modal" data-target="#modal">
                <div class="panel panel-default first_panel">
                    <a href="#"><img src="{{ asset('images/products/15a7cf0361679e.png') }}" style="width: 100%; height: 250px;"></a>
                </div>
            </div>
            <div class="col-md-3" data-toggle="modal" data-target="#modal">
                <div class="panel panel-default first_panel">
                    <a href="#"><img src="{{ asset('images/products/15a7dbd8e1dbc9.png') }}" style="width: 100%; height: 250px;"></a>
                </div>
            </div>
            <div class="col-md-3" data-toggle="modal" data-target="#modal">
                <div class="panel panel-default first_panel">
                    <a href="#"><img src="{{ asset('images/products/15a7cf85370f8c.png') }}" style="width: 100%; height: 250px;"></a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-3" data-toggle="modal" data-target="#modal">
                <div class="panel panel-default first_panel">
                    <a href="#"><img src="{{ asset('images/products/15a7dbe9fb6048.png') }}" style="width: 100%; height: 250px;"></a>
                </div>
            </div>
            <div class="col-md-3" data-toggle="modal" data-target="#modal">
                <div class="panel panel-default first_panel">
                    <a href="#"><img src="{{ asset('images/products/15a7dbf0a40a14.png') }}" style="width: 100%; height: 250px;"></a>
                </div>
            </div>
            <div class="col-md-3" data-toggle="modal" data-target="#modal">
                <div class="panel panel-default first_panel">
                    <a href="#"><img src="{{ asset('images/products/15a7dbfa8aa881.png') }}" style="width: 100%; height: 250px;"></a>
                </div>
            </div>
            <div class="col-md-3" data-toggle="modal" data-target="#modal">
                <div class="panel panel-default first_panel">
                    <a href="#"><img src="{{ asset('images/products/15a7cf85370f8c.png') }}" style="width: 100%; height: 250px;"></a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-3" data-toggle="modal" data-target="#modal">
                <div class="panel panel-default first_panel">
                    <a href="#"><img src="{{ asset('images/products/15a7cf1459f36b.png') }}" style="width: 100%; height: 250px;"></a>
                </div>
            </div>
            <div class="col-md-3" data-toggle="modal" data-target="#modal">
                <div class="panel panel-default first_panel">
                    <a href="#"><img src="{{ asset('images/products/15a7cf5568f8ed.png') }}" style="width: 100%; height: 250px;"></a>
                </div>
            </div>
            <div class="col-md-3" data-toggle="modal" data-target="#modal">
                <div class="panel panel-default first_panel">
                    <a href="#"><img src="{{ asset('images/products/15a7cf47909b35.png') }}" style="width: 100%; height: 250px;"></a>
                </div>
            </div>
            <div class="col-md-3" data-toggle="modal" data-target="#modal">
                <div class="panel panel-default first_panel">
                    <a href="#"><img src="{{ asset('images/products/15a7cf5568f8ed.png') }}" style="width: 100%; height: 250px;"></a>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/slider.js') }}"></script>
@endsection