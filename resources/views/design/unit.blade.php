@extends("layouts.public")
@section("contact")
    <link href="{{ asset('css/unit.css') }}" rel="stylesheet" >
<div class="container-fluid">
    <div class="row production text-center">
        <h3>Production Unit</h3>
    </div>
</div>
    <div class="container">
        <div class="row">
            <table>
                <tr>
                    <th>S.N</th>
                    <th>Name</th>
                    <th>Contact</th>
                    <th>Address</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>Milan Dulal</td>
                    <td>9841232524</td>
                    <td>Bhaktapur</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Santosh Bhattarai</td>
                    <td>9845659874</td>
                    <td>Kathmandu</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Suman Dhungana</td>
                    <td>9857856987</td>
                    <td>Dharan</td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Saroj Dangal</td>
                    <td>9865412030</td>
                    <td>Lalitpur</td>
                </tr>
                <tr>
                    <td>5</td>
                    <td>Ganesh Shrestha</td>
                    <td>98021326541</td>
                    <td>Banepa</td>
                </tr>
                <tr>
                    <td>6</td>
                    <td>Mukesh Shrestha</td>
                    <td>9842569874</td>
                    <td>Kavre</td>
                </tr>
            </table>
        </div>
    </div>


@endsection