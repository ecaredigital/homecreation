
<li> <a href="{{route("transaction.create")}}" class="waves-effect"><i class="fa fa-dollar fa-fw" data-icon="v"></i> Sell item </a></li>
{{--<li> <a href="#" class="waves-effect"><i class="fa fa-money fa-fw" data-icon="v"></i> <span class="hidDashboarde-menu"> Sell <span class="fa arrow"></span> </span></a>--}}
{{--<ul class="nav nav-second-level">--}}

{{--<li><a href="{{route("transaction.create")}}" >Item </a> </li>--}}
{{--<li><a href="{{route("transaction.index")}}" >History </a> </li>--}}
{{--</ul>--}}
{{--</li>--}}
<li> <a href="#" class="waves-effect"><i class="fa fa-cog fa-fw" data-icon="v"></i> <span class="hideDashboard-menu"> Setup <span class="fa arrow"></span> </span></a>
    <ul class="nav nav-second-level">

        <li> <a href="{{route("user.index")}}" class="waves-effect"> <span class="hide-menu">User Setup</span></a></li>
        <li> <a href="{{route("category.index")}}" class="waves-effect"> <span class="hide-menu">Category Setup</span></a></li>
        <li> <a href="{{route("subcategory.index")}}" class="waves-effect"> <span class="hide-menu">Subcategory Setup</span></a></li>
        <li> <a href="{{route("size.index")}}" class="waves-effect"> <span class="hide-menu">Size Setup</span></a></li>
        <li> <a href="{{route("quality.index")}}" class="waves-effect"> <span class="hide-menu">Quality Setup</span></a></li>
        <li> <a href="{{route("product.index")}}" class="waves-effect"> <span class="hide-menu">Product Setup</span></a></li>


    </ul>
</li>

<li><a href="#" class="waves-effect"><i class="fa fa-suitcase fa-fw"></i><span class="hideDashboard-menu"> Business Setup<span class="fa arrow"></span> </span> </a>
    <ul class="nav nav-third-level">
        <li><a href="{{url("admin/business")}}">Business</a> </li>
        <li><a href="{{url("admin/business_category")}}">Business Categories</a> </li>
        {{--<li><a href="{{route("admin.mission")}}">Mission</a> </li>--}}
    </ul>
</li>
<li><a href="{{route("order.index")}}"><i class="fa fa-shopping-cart fa-fw" aria-hidden="true"></i> Order Request </a> </li>
<li> <a href="{{route("stock.index")}}" class="waves-effect"><i class="fa fa-suitcase fa-fw" data-icon="v"></i> Current Stock </a></li>
<li><a href="{{route("admin.contact.index")}}"><i class="fa fa-envelope fa-fw"></i> Message</a> </li>
<li> <a href="#" class="waves-effect"><i class="fa fa-calendar fa-fw" data-icon="v"></i> <span class="hideDashboard-menu"> History <span class="fa arrow"></span> </span></a>
    <ul class="nav nav-second-level">

        <li><a href="{!! route("user.transaction.index") !!}"><i class="fa fa-user">&nbsp;HBW USER TRANSACTION</i> </a> </li>
        <li><a href="{{route("product.transaction.index")}}" >Product Stock</a> </li>
        <li><a href="{{route("transaction.report")}}">Report</a> </li>
        <li><a href="{{route("transaction.index")}}">Sell History</a></li>
    </ul>
</li>
<li><a href="#" class="waves-effect"><i class="fa fa-th-large fa-fw" aria-hidden="true"></i>  <span class="hideDashboard-menu"> Public <span class="fa arrow"></span> </span> </a>
    <ul class="nav nav-second-level">
        <li><a href="{{route("admin.slider.index")}}"><i class="fa fa-sliders" ></i> Slider Setup</a> </li>
        <li><a href="{{route("admin.feedback.index")}}"><i class="fa fa-comment fa-fw" ></i> Testimonial Setup</a> </li>
        <li><a href="{{route("admin.company.detail")}}"> <i class="fa fa-university" aria-hidden="true"></i>Company Detail Setup</a> </li>
        <li><a href="{{route("admin.worker.index")}}"><i class="fa fa-user fa-fw"></i> Employee profile Setup</a> </li>
    </ul>
</li>
<li><a href="{{url("admin/gallery")}}" class="waves-effect"><i class="fa fa-image fa-fw"></i> <span class="hideDashboard-menu"> Gallery</span></a> </li>
<li><a href="{{url("admin/event")}}"><i class="fa fa-th-list fa-fw"></i> Event</a> </li>
<li><a href="#" class="waves-effect"><i class="fa fa-cog fa-fw"></i>  <span class="hideDashboard-menu"> About us Setup<span class="fa arrow"></span> </span> </a>
    <ul class="nav nav-third-level">
        <li><a href="{{route("admin.about")}}">Company Overview setup</a> </li>
        <li><a href="{{route("admin.core")}}">Company Core about</a> </li>
        <li><a href="{{route("admin.mission")}}">Mission</a> </li>
        <li><a href="{{route("admin.vision")}}">Vision</a> </li>
        <li><a href="{{route("admin.objective")}}">Objective</a> </li>
    </ul>
</li>

<li><a href="{{url("admin/latest_activities")}}"><i class="fa fa-list fa-fw"></i>Latest Activities</a> </li>
<li><a href="{{url("admin/production_units")}}"><i class="fa fa-list fa-fw"></i>Production Unit</a> </li>


