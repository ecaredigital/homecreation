<section id="testimonial">
  <h3 class="text-center section-title">See what our client say</h3>
  <div class="row p-l-10">

      <div id="carousels3" class="carousel slide" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
          {{--<div class="item active">
            @foreach($testimonials as $testimonial)
            <div class="col-sm-3">
              <div class="card testimonial_div">
                <p class="text-center"><i class="fa fa-quote-left testimonial_icon"></i>
                  {{$testimonial->text}}<i class="fa fa-quote-right testimonial_icon"></i>
                </p>
                <div class="user-name">{{$testimonial->name}}</div>
                <div class="designation">{{$testimonial->company}}</div>
              </div>
            </div>--}}
          @foreach($testimonials->chunk(2) as $test)
            @if($loop->first)
              <div class="item active">
                @else
                  <div class="item">
                    @endif

                    @foreach($test as $testimonial)
                      <div class="col-sm-3 col-xs-4">
                        <div class="card testimonial_div">
                          <p class="text-center"><i class="fa fa-quote-left testimonial_icon"></i>
                            {{$testimonial->saying}}<i class="fa fa-quote-right testimonial_icon"></i>
                          </p>
                          <div class="user-name">{{$testimonial->name}}</div>
                          <div class="designation">{{$testimonial->company}}</div>
                        </div>
                      </div>
                    @endforeach
                  </div>
                  @endforeach
              </div>
        </div>
        <!-- Left and right controls -->
        <a class="left carousel-control" href="#carousels3" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousels3" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>

  </div>

</section>
{{--upcoming events starts from here--}}
<section id="featured">
  <h3 class="text-center section-title">Upcoming Events this week</h3>
  <div class="row m-t-10">
    @foreach($event as $event)
      <div class="col-sm-3 col-xs-6">
        <div class="row card upcoming_event_front">
          <h3 class="event-title">{{$event->title}}</h3>
          <span class="event_date"><i class="fa fa-calendar"></i>{{$event->start_date}} - {{$event->end_date}}</span>
          <span class="event_date"><i class="fa fa-clock-o"></i>{{$event->start_time}}-{{$event->end_time}}</span>
          <span class="event_details"><i class="fa fa-map-marker"></i> {{$event->venue}}</span><br>
          <span class="event_details">Organized by:<a href="#">{{$event->organized_by}}</a> </span><br>

          <p >{{ str_limit(strip_tags($event->details,300)) }}
            <a href="#" data-toggle="modal" data-target="#readmore{{$event->id}}"> ...Read more</a></p>
        </div>
      </div>
{{--modal for read more--}}
  <div class="modal fade" id="readmore{{$event->id}}" role="dialog">
    <div class="modal-dialog modal-lg">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">{{$event->title}}</h4>
        </div>
        <div class="modal-body">
          <span class="event_date"><i class="fa fa-calendar"></i>{{$event->start_date}} - {{$event->end_date}}</span>
          <span class="event_date"><i class="fa fa-clock-o"></i>{{$event->start_time}}-{{$event->end_time}}</span>
          <span class="event_details"><i class="fa fa-map-marker"></i> {{$event->venue}}</span>
            <span class="event_image"> <img src="{{asset($event->image)}}" class="img img-responsive" height="10" width="10"></span>
          <span class="event_details">Organized by:<a href="#">{{$event->organized_by}}</a> </span><br>
           <br>
          <p >{{strip_tags($event->details)}}</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>
    @endforeach
  </div>
  {{--end of modal for see more--}}
</section>
