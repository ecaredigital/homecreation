 */<section id="featured">
     <h3 class="text-center section-title">Special Offer</h3>
     <div class="row m-t-10">
         <div class="row ">
             <div id="carouselspecial" class="carousel slide" data-ride="carousel">
                 <!-- Wrapper for slides -->

                 <div class="carousel-inner">

                     @foreach($newly_arrival->chunk(4) as $item)
                         @if($loop->first)
                             <div class="item active">
                                 @else
                                     <div class="item">
                                         @endif

                                         @foreach($item as $product)
                                             <div class="col-sm-3 col-xs-4">
                                                 <div class="card">
                                                     <img src="{{$product->image}}" alt="" class="img img-responsive">
                                                     <div class="row m-t-5 ">
                                                         <div class="col-sm-9 col-xs-9 p-l-20">
                                                             <strong> {{$product->name}}</strong>
                                                         </div>
                                                     </div>
                                                     <a href="/product/{{$product->id}}" class="btn btn-success btn_center m-t-15 m-b-15">View Product</a>
                                                 </div>
                                             </div>
                                         @endforeach
                                     </div>
                                     @endforeach
                             </div>

                             <a class="left carousel-control" href="#carouselspecial" data-slide="prev">
                                 <span class="glyphicon glyphicon-chevron-left"></span>
                                 <span class="sr-only">Previous</span>
                             </a>
                             <a class="right carousel-control" href="#carouselspecial" data-slide="next">
                                 <span class="glyphicon glyphicon-chevron-right"></span>
                                 <span class="sr-only">Next</span>
                             </a>
                 </div>


             </div>
         </div>
     </div>

 </section>
