<link href="{{ asset('css/header.css') }}" rel="stylesheet">
<nav class="navbar navbar-default navbar-static-top TopNavbar">
    <div class="container-fluid white-text">
        <div class="navbar-header">

            <a class="navbar-brand white-text" href="/"> </a>
        </div>
        <div class="collapse navbar-collapse">
            <div class="row TopNavbar">
                <div class="col-md-7">
                <!-- Start Contact Info -->
                    <ul class="contact-details pull-right">
                        <li><a href=" "><i class="fa fa-map-marker">{{$global_company_details->city}},{{$global_company_details->district}} ,Nepal</i></a>
                        </li>
                        <li><a href="/contact"><i class="fa fa-envelope-o"></i> <span>{{$global_company_details->email}}</span></a>
                        </li>
                        <li><a href="#"><i class="fa fa-phone"></i> {{$global_company_details->phone_no}}</a>
                        </li>
                    </ul>
                    <!-- End Contact Info -->
                </div>
                <!-- .col-md-6 -->
                <div class="col-md-3">
                    <!-- Start Social Links -->
                    <ul class="social-list ">
                        <li>
                            <a href="{{$global_company_details->fb_link}}"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li>
                            <a href="{{$global_company_details->twitter_link}}"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li>
                            <a href="{{$global_company_details->linked_in_link}}"><i class="fa fa-linkedin"></i>    </a>
                        </li>
                        <li>
                            <a href="{{$global_company_details->insta_link}}"><i class="fa fa-instagram"></i></a>
                        </li>
                    </ul>
                    <!-- End Social Links -->
                </div>
                <!-- .col-md-6 -->
            </div>
        </div>
    </div>
</nav>

<nav class="navbar navbar-default navbar-static-top second_navbar">
    <div class="container-fluid white-text">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand white-text" href="/" style="padding:0;"><img src="{{asset($global_company_details->logo_url)}}" class="img img-responsive brand_logo"> </a>

        </div>

        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav bordeer_snip pull-right">
                <li class="current"><a href="/"> Home</a></li>
                <li class="dropdown"><a href="/about" dropbtn>About Us<span class="caret"></span></a>
                <ul class="dropdown-content">
                    <li><a href="/mission">Mission</a></li>
                    <li><a href="/objective">Objectives</a></li>
                    <li><a href="/vision">Vision</a></li>
                    <li><a href="/production_units">Production Unit</a></li>
                </ul>
                </li>
                <li class="dropdown"><a href="/products">Product<span class="caret"></span></a>
                    <ul class="dropdown-content">
                        <li><a href="{{url("/newly_arrived")}}">Newly Arrived</a></li>
                        <li><a href={{url("/special_offer")}}>Special offer</a></li>
                        {{--<li class="dropdown dropdown-submenu"><a href="#">Categories<span class="fa fa-caret-right pull-right"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Category1</a></li>
                                <li><a href="#">Category2</a></li>
                                <li><a href="#">Category3</a></li>
                                <li><a href="#">Category4</a></li>
                               <span class="pull-right"> <a href="#" class="see_more">See More</a></span>
                            </ul>
                        </li>--}}
                    </ul>
                </li>

                <li class="dropdown"><a href="#" >Business<span class="caret"></span></a>
                        <ul class="dropdown-content">
                            <li><a href="{{url("/StakeHolder")}}">Major stake holders </a></li>
                            <li class="dropdown dropdown-submenu"><a href="{{url("/DealWithPatner")}}">Deal with Partners <span class="fa fa-caret-right pull-right"></span></a>

                                <ul class="dropdown-menu">
                                    @foreach($global_Business as $business)
                                    <li class="white-text"><a href="/DealWithPatnerid/{{$business->id}}">MOU with {{$business->title}}</a></li>
                                    {{--<li class="white-text"><a href="#">Category1</a></li>--}}
                                    {{--<li><a href="#">Category2</a></li>--}}
                                    {{--<li><a href="#">Category3</a></li>--}}
                                    {{--<li><a href="#">Category4</a></li>--}}
                                    {{--<span class="pull-right"> <a href="#" class="see_more">See More</a></span>--}}
                                    @endforeach

                                </ul>

                            </li>
                            <li><a href="{{url("/latest_activity")}}">Latest Activities </a></li>
                        </ul>
                </li>
                <li><a href="{{url("/upcoming_event")}}"> Upcoming Events</a></li>
                <li><a href="{{url("/gallery")}}"> Gallery</a></li>
                <li><a href="/contact"> Contact</a></li>
                <li><a href="{{url("/testimonial")}}"> Customer Feedback</a></li>
            </ul>
        </div>

    </div>
</nav>