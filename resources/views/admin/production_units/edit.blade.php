@extends("layouts.inventory")

@section("content")

    <div class="col-sm-12 m-t-5 m-b-5">
        <a href="{{route("admin.production_units.index")}}" class="btn btn-primary">All Production Units</a>
    </div>
    <div class="row white-box m-t-20">
        <h2 class="page-header text-center">Edit Id:{{$units->id}} </h2>
        <div class="col-sm-12">
            <form method="post" action="{{route("admin.production_units.update",$units->id)}}" class="form-horizontal" enctype="multipart/form-data">
                {{csrf_field()}}
                {{method_field("put")}}
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-2 control-label">Name </label>

                    <div class="col-md-8">
                        <input id="name" type="text" class="form-control" name="name"  value="{{$units->name}}"required autofocus>

                        @if ($errors->has('name'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-2 control-label">Address </label>

                    <div class="col-md-8">
                        <input id="address" type="text" class="form-control" name="address" value="{{$units->address}}" required autofocus>

                        @if ($errors->has('address'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('contact') ? ' has-error' : '' }}">
                    <label for="contact" class="col-md-2 control-label">Contact </label>

                    <div class="col-md-8">
                        <input id="contact" type="number" class="form-control" name="contact" value="{{$units->contact}}" required autofocus>

                        @if ($errors->has('contact'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('contact') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <input type="submit" class="btn btn-success pull-right" value="Update">
                </div>

            </form>
        </div>
    </div>
@endsection