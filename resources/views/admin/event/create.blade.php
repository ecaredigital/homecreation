@extends("layouts.inventory")
@section("style")
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
@endsection
@section("content")
    <div class="row white-box m-t-20">
        <h2 class="page-header text-center">Add a new Event</h2>
        <div class="col-sm-12">
            <form method="post" action="{{route("admin.event.store")}}" class="form-horizontal" enctype="multipart/form-data">
                {{csrf_field()}}

                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    <label for="title" class="col-md-2 control-label">Title</label>

                    <div class="col-md-10">
                        <input id="title" type="text" class="form-control" name="title"  required autofocus>

                        @if ($errors->has('title'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>


                <div class="form-group{{ $errors->has('start_time') ? ' has-error' : '' }}">
                    <label for="time" class="col-md-2 control-label">Start Time</label>

                    <div class="col-md-10">
                        <input  id="start_time"  name="start_time" class="timepicker form-control" type="time" >

                        @if ($errors->has('start_time'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('start_time') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('end_time') ? ' has-error' : '' }}">
                    <label for="end_time" class="col-md-2 control-label">End Time</label>

                    <div class="col-md-10">
                        <input  id="end_time"  name="end_time" class="timepicker form-control" type="time">

                        @if ($errors->has('end_time'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('end_time') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
                    <label for="start-date" class="col-md-2 control-label">Start Date</label>

                    <div class="col-md-10">
                        <input id="start_date" type="date" class="form-control" name="start_date"  required >

                        @if ($errors->has('start_date'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('start_date') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
                    <label for="end_date" class="col-md-2 control-label">End Date</label>

                    <div class="col-md-10">
                        <input id="end_date" type="date" class="form-control" name="end_date"  >

                        @if ($errors->has('end_date'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('end_date') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('venue') ? ' has-error' : '' }}">
                    <label for="venue" class="col-md-2 control-label">Venue</label>

                    <div class="col-md-10">
                        <input id="venue" type="text" class="form-control" name="venue"  required >

                        @if ($errors->has('venue'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('venue') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('organized_by') ? ' has-error' : '' }}">
                    <label for="organized_by" class="col-md-2 control-label">organized_by</label>

                    <div class="col-md-10">
                        <input id="organized_by" type="text" class="form-control" name="organized_by"  required >

                        @if ($errors->has('organized_by'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('organized_by') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('details') ? ' has-error' : '' }}">
                    <label for="company" class="col-md-2 control-label">Details</label>
                    <div class="col-md-10">
                        <textarea name="note" id="editor">Deatils here</textarea>

                        @if ($errors->has('details'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('details') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                    <label for="image" class="col-md-2 control-label">Image </label>

                    <div class="col-md-10">
                        <input id="image" type="file" class="form-control" name="image" autofocus>

                        @if ($errors->has('image'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>


                <div class="form-group">
                    <input type="submit" class="btn btn-success pull-right" value="Create">
                </div>

            </form>
        </div>
    </div>
@endsection
@section("script")
    <script src="https://cdn.ckeditor.com/ckeditor5/11.1.1/classic/ckeditor.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>


    <script type="text/javascript">
        $('.timepicker').datetimepicker({
            format: 'HH:mm:ss'
        });
    </script>
    <script>
        ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .then( editor => {
                console.log( editor );
            } )
            .catch( error => {
                console.error( error );
            } );
    </script>

@endsection