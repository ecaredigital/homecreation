@extends("layouts.inventory")
@section("content")
    <div class="row">
        <div class="col-sm-12 m-t-5 m-b-5">
            <a href="{{route("admin.event.create")}}" class="btn btn-primary">Add Events </a>
        </div>
        <div class="col-sm-12">
            <table class="table table-bordered table-responsive">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Start Time</th>
                    <th>End Time</th>
                    <th>Image</th>
                    <th>Venue</th>
                    <th>Organized By</th>
                    <th>Details</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($events as $event)
                    <tr>
                        <td>{{$event->id}}</td>
                        <td>{{$event->title}}</td>
                        <td>{{$event->start_date}}</td>
                        <td>{{$event->end_date}}</td>
                        <td>{{$event->start_time}}</td>
                        <td>{{$event->end_time}}</td>
                        <td><img src="{{asset($event->image)}}" class="img img-responsive" height="200" width="200"> </td>
                        <td>{{$event->venue}}</td>
                        <td>{{$event->organized_by}}</td>
                        <td>{{strip_tags($event->details)}}</td>

                        <td>
                            <a href="{{route("admin.event.edit",$event->id)}}" class="btn btn-success"> Edit</a>
                            <a href="#" onclick="deleteSlider('{{$event->id}}')" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Delete </h4>
                    </div>
                    <div class="modal-body">
                        <p>You are on the delete track this process is irreversible. Are you sure? </p>
                    </div>
                    <div class="modal-footer">
                        <form action="#" id="deleteSlider"  method="post">
                            {{csrf_field()}}
                            {{method_field("delete")}}
                            <button type="submit" class="btn btn-danger">Delete</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </form>

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script>
        function deleteSlider(id) {
            $("#deleteSlider").attr("action","/admin/event/"+id);
            $("#myModal").modal("show")
        }
    </script>
@endsection