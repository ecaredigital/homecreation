@extends("layouts.inventory")
@section("styles")
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
@endsection
@section("content")


    <div class="row white-box m-t-20">
        <div class="col-sm-12 m-t-5 m-b-5">
        <a href="{{route("admin.business.index")}}" class="btn btn-primary">All </a>
        </div>
        <h2 class="page-header text-center">Edit Id:{{$business->id}} </h2>
        <div class="col-sm-12">
            <form method="post" action="{{route("admin.business.update",$business->id)}}" class="form-horizontal" enctype="multipart/form-data">
                {{csrf_field()}}
                 {{method_field("PUT")}}

                <div class="form-group {{ $errors->has('category_id') ? ' has-error' : '' }}">
                    <label for="title" class="col-md-2 control-label">Category </label>
                    <div class="col-md-10">
                        <select name="category_id" class="form-control" >

                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('category_id'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('category_id') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    <label for="title" class="col-md-2 control-label">Title </label>

                    <div class="col-md-10">
                        <input id="title" type="text" class="form-control" name="title" value="{{$business->title}}"  required >

                        @if ($errors->has('title'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                    <label for="description" class="col-md-2 control-label">Description</label>

                    <div class="col-md-10">
                        <textarea id="summernote" name="note">{{$business->description}}"</textarea>

                        @if ($errors->has('description'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>



                <div class="form-group">
                    <input type="submit" class="btn btn-success pull-right" value="Update">
                </div>

            </form>
        </div>
    </div>
@endsection

@section("script")
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote({
                height: 300,
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']]
                ]
            });
        });
    </script>
@endsection