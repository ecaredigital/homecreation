@extends("layouts.inventory")
@section("content")
    <div class="col-sm-12 m-t-5 m-b-5">
        <a href="{{route("admin.business_category.index")}}" class="btn btn-primary">All Category</a>
    </div>
    <div class="row white-box m-t-20">
        <h2 class="page-header text-center">Edit Category</h2>
        <div class="col-sm-10">
            <form method="post" action="{{route("admin.business_category.update",$categories->id)}}" class="form-horizontal" enctype="multipart/form-data">
                {{csrf_field()}}
                {{method_field("PUT")}}
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-2 control-label">Category</label>

                    <div class="col-md-10">
                        <input id="name" type="text" class="form-control" name="name" value="{{ $categories->name }}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <input type="submit" class="btn btn-success pull-right" value="Update">
                </div>

            </form>
        </div>
    </div>
@endsection