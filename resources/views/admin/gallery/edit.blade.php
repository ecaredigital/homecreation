@extends("layouts.inventory")
@section("styles")
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
@endsection
@section("content")
    <div class="row white-box m-t-20">
        <div class="col-sm-12 m-t-5 m-b-5">
            <a href="{{url("admin/gallery/create")}}" class="btn btn-primary">Add Images</a>
            <a href="{{route("admin.gallery.index")}}" class="btn btn-primary">All Images</a>
        </div>
        <h2 class="page-header text-center">Edit Id:{{$galleries->id}} </h2>
        <div class="col-sm-10">
            <form method="post" action="{{route("admin.gallery.update",$galleries->id)}}" class="form-horizontal" enctype="multipart/form-data">
                {{csrf_field()}}
                {{method_field("put")}}

                <img src="{{asset('/images/gallery/'.$galleries->filename)}}" class="img img-responsive" height="200" width="200">
                <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                    <label for="file" class="col-md-2 control-label">Image </label>

                    <div class="col-md-10">
                        <input id="file" type="file" class="form-control" name="file"  >

                        @if ($errors->has('file'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('file') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                    <label for="description" class="col-md-2 control-label">Description</label>

                    <div class="col-md-10">
                        <textarea id="summernote" name="note">{{$galleries->description}}</textarea>

                        @if ($errors->has('description'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-success pull-right" value="update">
                </div>

            </form>
        </div>
    </div>

@endsection

@section("script")
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote({
                height: 300,
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']]
                ]
            });
        });
    </script>
@endsection