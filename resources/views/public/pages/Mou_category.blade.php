
@extends("layouts.public")
@section("contact")
    <link href="{{ asset('css/category.css') }}" rel="stylesheet" >
    <div class="container">
        <div class="row">
            <div class="header">
                <h4>{{$business->title}}</h4>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                    </div>
                    <div class="row detail">
                        {{--<span class="date"><i class="fa fa-calendar"></i> Wed, 21 - Sat, 24 Nov 2018</span>--}}
                        {{--<span class="time"><i class="fa fa-clock-o"></i> 1:00 AM</span>--}}
                        {{--<span class="hotel"><i class="fa fa-map-marker"></i> Hotel Yak and Yeti, Kathmandu</span><br>--}}
                        {{--<span class="details">Organized by:<a href="#"> Home Creation</a> </span><br>--}}
                        <span class="details"><p>{{strip_tags($business->description)}}</p></span>
                    </div>
                    <div class="row footer">

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection