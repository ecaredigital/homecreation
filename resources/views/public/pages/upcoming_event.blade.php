@extends("layouts.public")

@section("style")
<link href="{{ asset('css/sub_heading.css') }}" rel="stylesheet">
<link href="{{ asset('css/upcoming_events.css') }}" rel="stylesheet">
@endsection
@section("contact")
    <div class="sub_heading_title">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-12">
                    <h1 class="h1 text-center">
                        <b>Upcoming Events</b></h1>
                </div>
            </div>
        </div>
    </div>
    @foreach($events as $event)
                <div class="container">
                    <div class="paragraph">
                <div class="row card upcoming_event">
                   <h3 class="event-title">{{$event->title}}</h3>
                   <span class="event_date"><i class="fa fa-calendar"></i> {{$event->start_date}} - {{$event->end_date}}</span>
                   <span class="event_date"><i class="fa fa-clock-o"></i> {{$event->start_time}}-{{$event->end_time}}</span>
               <span class="event_venue"><i class="fa fa-map-marker"></i> {{$event->venue}}</span><br>
               <span class="event_details">Organized by:<a href="#">{{$event->organized_by}}</a> </span><br>

               <p>{{ str_limit(strip_tags($event->details,300)) }}
                   <a href="#" data-toggle="modal" data-target="#readmore{{$event->id}}"> ...Read more</a></p>
               </p>
           </div>
                    </div>
                </div>
                        {{--modal for read more--}}
    <div class="modal fade" id="readmore{{$event->id}}" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{$event->title}}</h4>
                </div>
                <div class="modal-body">
                    <span class="event_date"><i class="fa fa-calendar"></i>{{$event->start_date}} - {{$event->end_date}}</span>
                    <span class="event_date"><i class="fa fa-clock-o"></i>{{$event->start_time}}-{{$event->end_time}}</span>
                    <span class="event_details"><i class="fa fa-map-marker"></i> {{$event->venue}}</span>
                    <span class="event_image"> <img src="{{asset($event->image)}}" class="img img-responsive" height="100" width="100"></span><br>
                    <span class="event_details">Organized by:<a href="#">{{$event->organized_by}}</a> </span><br>
                    <p >{{strip_tags($event->details)}}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    @endforeach
       {{-- </div>
    </div>--}}
    {{--end of modal for see more--}}
@endsection
