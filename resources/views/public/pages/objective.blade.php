@extends("layouts.public")
<link href="{{ asset('css/sub_heading.css') }}" rel="stylesheet">


@section("contact")
    <div class="sub_heading_title">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-12">
                    <h1 class="h1 text-center">
                        <b>Our Objective</b></h1>
                    <h2>WHY US ??</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="paragraph">
            <p>
            <div>

                {!! $global_company_details->objective !!}
            </div>
            </p>
        </div>
    </div>

@endsection
