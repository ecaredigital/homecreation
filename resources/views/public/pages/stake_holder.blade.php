@extends("layouts.public")
<link href="{{ asset('css/sub_heading.css') }}" rel="stylesheet">
<link href="{{ asset('css/stake_holder.css') }}" rel="stylesheet">
{{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--}}
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


@section("content")
    <div class="sub_heading_title">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-12">
                    <h1 class="h1 text-center">
                        <b>Stake Holders</b></h1>
                </div>
            </div>
        </div>
    </div>
    @foreach($business as $business)
    <div class="container">
        <div class="row card stake_holder">
            <h3 class="event-title">{{$business->title}}</h3>
            <span class="event_details"> </span><br>
            <p> {{ str_limit(strip_tags($business->description,300)) }}</p>
                <a href="#" data-toggle="modal" data-target="#readmore{{$business->id}}" class="modal-fade"> ...Read more</a></p>

        </div>

    </div>
    {{--modal for read more--}}
    <div class="modal fade" id="readmore{{$business->id}}" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" >
                    <h4 class="modal-title">{{strip_tags($business->title)}}</h4>
                </div>
                <div class="modal-body">
                    <p >{{strip_tags($business->description)}}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    @endforeach
    {{--end of modal for see more--}}
@endsection

@section("script")

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
@endsection