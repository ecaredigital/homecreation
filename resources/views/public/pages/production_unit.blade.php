@extends("layouts.public")
@section("contact")
    <link href="{{ asset('css/unit.css') }}" rel="stylesheet" >
<div class="container-fluid">
    <div class="row production text-center">
        <h3>Production Unit</h3>
    </div>
</div>
    <div class="container">
        <div class="row">
            <table>
                <tr>
                    <th>S.N</th>
                    <th>Name</th>
                    <th>Contact</th>
                    <th>Address</th>
                </tr>
                 @foreach($production_unit as $unit)
                <tr>
                    <td>{{$unit->id}}</td>
                    <td>{{$unit->name}}</td>
                    <td>{{$unit->contact}}</td>
                    <td>{{$unit->address}}</td>
                </tr>
                     @endforeach

            </table>
        </div>
    </div>


@endsection