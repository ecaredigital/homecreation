@extends("layouts.public")
@section("contact")

    <link href="{{ asset('css/activity.css') }}" rel="stylesheet" >
<div class="container-fluid">
    <div class="row text-center activity">
        <h4>Latest Activity</h4>
    </div>
</div>
    <div class="container">
        <div class="row">
            @foreach($activites as $activity)
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"><strong>{{$activity->title}}</strong></h4>

                    <p class="card-text">{{strip_tags($activity->description)}}</p>
                </div>
            </div>
                @endforeach
        </div>
    </div>

@endsection