@extends("layouts.public")
@section("contact")
    <link href="{{ asset('css/gallery.css') }}" rel="stylesheet" >
<div class="container-fluid">
    <div class="row text-center photo">
        <h4>Gallery</h4>
    </div>
</div>

    <div class="container first_line">
        <div class="row">
            @foreach($gallery as $gallery)
             <div class="col-md-3" data-toggle="modal" data-target="#modal{{$gallery->id}}">
                    <div class="panel panel-default first_panel">
                        <a href="#"><img src="{{ asset('images/gallery/'.$gallery->filename) }}" style="width: 100%; height: 250px;"></a>
                    </div>
                </div>
                <div class="modal fade" role="dialog" id="modal{{$gallery->id}}">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button class="close" data-dismiss="modal">X</button>
                                <h3>Home Creation Gallery</h3>
                            </div>
                            <div class="modal-body">
                                <img src="{{ asset('images/gallery/'.$gallery->filename) }}">
                                <p>{{strip_tags($gallery->description)}}</p>
                                </div>
                            <div class="modal-footer">
                                Gallery
                            </div>
                        </div>
                    </div>
                </div>
        @endforeach
    </div>
    </div>


    <script src="{{ asset('js/slider.js') }}"></script>
@endsection