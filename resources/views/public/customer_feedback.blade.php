@extends("layouts.public")
<link href="{{ asset('css/sub_heading.css') }}" rel="stylesheet">
<link href="{{ asset('css/customer_feedback.css') }}" rel="stylesheet">


@section("content")
    <div class="sub_heading_title">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-12">
                    <h1 class="h1 text-center">
                        <b>Customer Feedback</b></h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="paragraph">
           <div class="row testimonial">
               @foreach($testimonials as $testimonial)
               <div class="col-md-3">
                   <div class="box_shadow card">
                     <i><p><i class="fa fa-quote-left fa-2x testimonial_icon"></i>
                              {{$testimonial->saying}}<i class="fa fa-quote-right fa-2x testimonial_icon"></i></p>
                           <div class="contact">
                                <span class="user-name">{{$testimonial->name}}</span><br>
                               <span class="grey">{{$testimonial->company}}</span>
                           </div>
                     </i>
                   </div>
               </div>
                   @endforeach
           </div>
        </div>
    </div>

@endsection
