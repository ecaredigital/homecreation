@extends("layouts.public")
@section("styles")
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link href="{{ asset('css/newly_arrive.css') }}" rel="stylesheet" >
@endsection
@section("content")

    <div class="container-fluid demo-3">
        <div class="content">
            <div id="large-header" class="large-header">
                <canvas id="demo-canvas"></canvas>
                <h1 class="main-title"><span>Newly Arrive</span></h1>
            </div>
        </div>
    <div class="row">

        <div class="row">
            <div class="col-sm-3 " style="margin-top: 5rem; padding-left: 3rem;">
                <sidebar :sub_id="{!!request('sub',0)!!}" :sort="'{!!request('sort','latest')!!}'"></sidebar>
            </div>

            <div class="col-sm-9">
                <div class="row">
                    @foreach ($products as $key => $product)
                        <div class="col-sm-4 col-xs-6 m-t-10">
                            <img src="{{asset($product->image)}}" alt="" class="img img-responsive">
                            <div class="offer">
                                <img src="{{ asset('images/products/Untitled-2.gif') }}" style="width:56%;height: 20%;">
                            </div>

                            <strong>Name : {{$product->name}}</strong>
                            <br>

                            <strong>Price : NPR {{$product->price}}</strong>



                            <a href="/product/{{$product->id}}" class="btn btn-success btn-block m-t-5">View Product</a>
                        </div>
                    @endforeach

                </div>
                </div>
        </div>
    </div>

    </div>


@endsection


@section("scripts")

    <script>

        new Vue({
            el:"#wrapper"
        });
        $(document).ready(function(){
// invoke the carousel
            $('#myCarousel').carousel({
                interval:4000
            });
            $("#myCarousel").on("touchstart", function(event){

                var yClick = event.originalEvent.touches[0].pageY;
                $(this).one("touchmove", function(event){

                    var yMove = event.originalEvent.touches[0].pageY;
                    if( Math.floor(yClick - yMove) > 1 ){
                        $(".carousel").carousel('next');
                    }
                    else if( Math.floor(yClick - yMove) < -1 ){
                        $(".carousel").carousel('prev');
                    }
                });
                $(".carousel").on("touchend", function(){
                    $(this).off("touchmove");
                });
            });

        });
        //animated  carousel start
        $(document).ready(function(){

//to add  start animation on load for first slide
            $(function(){
                $.fn.extend({
                    animateCss: function (animationName) {
                        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
                        this.addClass('animated ' + animationName).one(animationEnd, function() {
                            $(this).removeClass(animationName);
                        });
                    }
                });
                $('.item1.active img').animateCss('slideInDown');
                $('.item1.active h2').animateCss('zoomIn');
                $('.item1.active p').animateCss('fadeIn');

            });

//to start animation on  mousescroll , click and swipe



            $("#myCarousel").on('slide.bs.carousel', function () {
                $.fn.extend({
                    animateCss: function (animationName) {
                        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
                        this.addClass('animated ' + animationName).one(animationEnd, function() {
                            $(this).removeClass(animationName);
                        });
                    }
                });

// add animation type  from animate.css on the element which you want to animate
                //
                $('.item1 img').animateCss('slideInDown');
                $('.item1 h2').animateCss('zoomIn');
                $('.item1 p').animateCss('fadeIn');

                $('.item2 img').animateCss('zoomIn');
                $('.item2 h2').animateCss('swing');
                $('.item2 p').animateCss('fadeIn');

                $('.item3 img').animateCss('fadeInLeft');
                $('.item3 h2').animateCss('fadeInDown');
                $('.item3 p').animateCss('fadeIn');
            });
        });

    </script>
    <script src="{{ asset('js/TweenLite.min.js') }}"></script>
    <script src="{{ asset('js/EasePack.min.js') }}"></script>
    <script src="{{ asset('js/demo-3.js') }}"></script>
@endsection
