@extends("layouts.inventory")

@section("content")

    <div class="row" >
        <div class="col-md-12 ">

            <a href="{{route("product.create")}}" class="btn btn-primary" >Add a new product</a>
            <a href="{{route("product.index")}}" class="btn btn-primary" >All Product</a>
            <a href="{{route("product.newly_arrival")}}" class="btn btn-primary" >Newly Arrive</a>
            <a href="{{route("product.special_offer")}}" class="btn btn-primary" >Special Offer</a>

            <table class="table table-responsive white-box m-t-20" v-cloak>
                <thead>
                <tr>
                    <th>#</th>

                    <th>Category</th>
                    <th>Name</th>
                    <th>Image</th>
                    <th>Price</th>
                    <th>Action</th>
                </tr>
                {{--</thead>--}}
                {{--<tbody>--}}
                {{--<tr v-for="product in products">--}}
                    {{--<td>@{{ product.id }}</td>--}}
                    {{--<td>@{{ product.category.name }}</td>--}}
                    {{--<td>@{{ product.name }}</td>--}}
                    {{--<td><img :src="'/'+product.image" height="100" width="100"></td>--}}
                    {{--<td>Npr.&nbsp; @{{ product.price }}</td>--}}
                    {{--<td>--}}
                        {{--<a class="btn btn-success"  :href="'/inventory/product/'+product.id+'/edit'" >Edit</a>--}}
                        {{--<button class="btn btn-danger" @click="deleteProduct(product.id)">Delete</button> </td>--}}
                {{--</tr>--}}
                {{--</tbody>--}}
            {{--</table>--}}

                </thead>
                <tbody>
                @foreach($products as $product)
                <tr>
                    <td>{{$product->id}}</td>
                    <td>
                        @foreach($categories as $category)
                            @if($category->id == $product->category_id)
                                {{$category->name}}
                            @endif
                        @endforeach
                    </td>
                    <td>{{ $product->name }}</td>
                    <td>
                        <img src="{{asset("$product->image")}}" height="100" width="100">
                    </td>
                    <td>{{ $product->price }}</td>
                    <td>
                        <a href="{{route("product.edit",$product->id)}}"  class="btn btn-primary">Edit</a>
                        <a href="#" class="btn btn-danger" onclick="deleteService('{{$product->id}}')">Delete </a>
                    </td>
                </tr>
                    @endforeach
                </tbody>
            </table>


            <div class="modal fade"  id="myDeleteModal">
                <div class="modal-dialog modal-confirm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="icon-box">
                                <i class="fa fa-times-circle"></i>
                            </div>
                            <h4 class="modal-title">Are you sure?</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <p>Do you really want to delete these records? This process cannot be undone.</p>
                        </div>
                        <div class="modal-footer">
                            <form method="post" action="#" id="myDeleteModalForm">
                                {{csrf_field()}}
                                {{method_field("DELETE")}}
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-danger float-right">Delete</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section("script")

    <script>
        function deleteService(id){
            $("#myDeleteModalForm").attr({
                action:'/inventory/product/'+id
            });
            $("#myDeleteModal").modal("show")

        }
    </script>
    <script>
        new Vue({
            el:"#wrapper",
            data:{
                sub:{!!request('sub',0)!!},
                sort:'{!!request('sort','latest')!!}'
            },
            methods:{
                changeProduct:function(){
                    window.location.href="/products?sub="+this.sub+"&sort="+this.sort;
                }
            }
        })
    </script>

@endsection