<?php

namespace App\Providers;

use App\Model\CompanyDetail;
use App\Model\Business;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        $all_details=CompanyDetail::first();
        $business=Business::select("id","title","description","business_category_id")->where("business_category_id",'=',1)->get();

        view::Share("global_Business",$business);
        View::share("global_company_details",$all_details);
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
