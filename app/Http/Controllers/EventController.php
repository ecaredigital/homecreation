<?php

namespace App\Http\Controllers;

use App\Model\Event;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events=Event::all();
        return view("admin.event.index",compact("events"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.event.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            "title"=>"required",
            ]);

        if($request->hasFile("image")) {
            $file = $request->file("image");
            $filename = uniqid(true) . '.png';
            $db_location = 'images/events/' . $filename;
            $location = public_path('images/events/' . $filename);

            Image::make($file)->encode("png")->resize(640, 360, function ($constraint) {
                $constraint->aspectRatio();
            })->save($location);
            Event::create([
                "title" => $request->get("title"),
                "start_date" => $request->get("start_date"),
                "end_date" => $request->get("end_date"),
                "image" => $db_location,
                "start_time" => $request->get("start_time"),
                "end_time" => $request->get("end_time"),
                "venue" => $request->get("venue"),
                "organized_by" => $request->get("organized_by"),
                "details" => $request->get("note"),

            ]);
        }
        else{
            Event::create([
                "title" => $request->get("title"),
                "start_date" => $request->get("start_date"),
                "end_date" => $request->get("end_date"),
                "start_time" => $request->get("start_time"),
                "image" => "no image",
                "end_time" => $request->get("end_time"),
                "venue" => $request->get("venue"),
                "organized_by" => $request->get("organized_by"),
                "details" => $request->get("note"), ]);
        }
        Session::flash("success","Event added successfully");

        return redirect()->route("admin.event.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event=Event::findorfail($id);
//        dd($event);
        return  view("admin.event.edit",compact("event"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $events=Event::findorfail($id);
        $this->validate($request,[


        ]);

        if($request->hasFile("image")){

            $old_file=public_path($events->image);

            if(file_exists($old_file))
                unlink($old_file);
            $file=$request->file("image");
            $filename=uniqid(true).'.png';
            $db_location='images/events/'.$filename;

            $location=public_path('images/events/'.$filename);

            Image::make($file)->encode("png")->resize(640, 360, function ($constraint) {
                $constraint->aspectRatio();
            })->save($location);
            $events->update([
                "title"=>$request->get("title"),
                "start_date"=>$request->get("start_date"),
                "end_date"=>$request->get("end_date"),
                "image"=>$db_location,
                "start_time"=>$request->get("start_time"),
                "end_time"=>$request->get("end_time"),
                "venue"=>$request->get("venue"),
                "organized_by"=>$request->get("organized_by"),
                "details"=>$request->get("note"),
            ]);

        }else{
            $events->update([
                "title"=>$request->get("title"),
                "start_date"=>$request->get("start_date"),
                "end_date"=>$request->get("end_date"),
                "start_time"=>$request->get("start_time"),
                "end_time"=>$request->get("end_time"),
                "venue"=>$request->get("venue"),
                "organized_by"=>$request->get("organized_by"),
                "details"=>$request->get("note"),
            ]);

        }
        Session::flash("success","Events updated successfully");

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $events=Event::findorfail($id);
        $events->delete();
        Session::flash("success","Events has been deleted");
        return redirect()->back();
    }
}
