<?php

namespace App\Http\Controllers;
use App\Model\ImageUpload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ImageUploadController extends Controller
{

    public function index()
    {
        $galleries=ImageUpload::all();
        return view('admin.gallery.index',compact("galleries"));
    }
    public function fileCreate()
    {
        return view('admin.gallery.create');
    }
    public function fileStore(Request $request)
    {
        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $image->move(public_path('images\gallery'),$imageName);
        $imageUpload = new ImageUpload();
        $imageUpload->filename = $imageName;
        $imageUpload->save();
        return response()->json(['success'=>$imageName]);
    }

    public function edit($id)
    {
        $galleries=ImageUpload::findorfail($id);
        return view("admin.gallery.edit",compact("galleries"));
    }



    public function update(Request $request, $id)
    {
        $gallery=ImageUpload::findorfail($id);
        $this->validate($request,[
            "filename"=>"nullable|mimes:jpg,jpeg,png",
            "description"=>"nullable|max:1000",
        ]);

        if($request->hasFile("file")){
            $old_file=public_path('/images/gallery/'.$gallery->filename);

            if(file_exists($old_file))
                unlink($old_file);
            $file=$request->file("file");
            $filename=$file->getClientOriginalName();
            $file->move(public_path('images\gallery/'),$filename);

            $gallery->update([

                "filename"=>$filename,
                "description"=>$request->get("note"),
            ]);

        }else{
            $gallery->update([
                "description"=>$request->get("note"),
            ]);

        }
        Session::flash("success","Image updated successfully");

        return redirect()->back();
    }

    public function destroy($id)
    {
        $gallery=ImageUpload::findorfail($id);

        $location=public_path('/images/gallery/'.$gallery->filename);

        if(file_exists($location))
            unlink($location);
        $gallery->delete();

        Session::flash("success","Image deleted successfully");
        return redirect()->back();
    }
    public function fileDestroy(Request $request)
    {
        $filename =  $request->get('filename');
        ImageUpload::where('filename',$filename)->delete();
        $path=public_path().'/images/gallery/'.$filename;
        if (file_exists($path)) {
            unlink($path);
        }
        return $filename;
    }
}
