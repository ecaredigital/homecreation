<?php

namespace App\Http\Controllers\Front;

use App\Model\Business;
use App\Model\BusinessCategory;
use App\Model\CompanyDetail;
use App\Model\CompanyLeader;
use App\Model\ContactMessage;
use App\Model\Event;
use App\Model\Feedback;
use App\Model\ImageUpload;
use App\Model\LatestActivities;
use App\Model\Product;
use App\Model\ProductionUnits;
use App\Model\Slider;
use App\Model\Testimonial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Model\SubCategory;

class PageController extends Controller
{
    public function getContact()
    {
        return view("public.pages.contact");
    }

    public function getIndex()
    {
//        $categories = BusinessCategory::all();
//        $business=Business::select("id","title","description","business_category_id")->where("business_category_id",'=',1)->get();
        $sliders=Slider::select("id", "name", "image_url")->get();
        $top_products=Product::select("id", "name", "price", "image")->where("status", 1)->inRandomOrder()->get();
        $featured=Product::select("id", "name", "price", "image")->where("status", 2)->inRandomOrder()->get();
        $testimonials=Feedback::select("id", "name", "position", "company", "image_url","saying")->where("status",1)->inRandomOrder()->take(4)->get();
        $newly_arrival=Product::select("id","name","image","price","code","category_id")->where("newly_arrival",'=',1)->inRandomOrder()->get();
        $special_offer=Product::select("id","name","image","price","code","category_id")->where("special_offer",'=',1)->inRandomOrder()->get();
        $event=Event::all();

        return view("public.index", compact("sliders", "top_products", "featured", "testimonials","newly_arrival","special_offer","business","categories","event"));
    }


    public function storeContact(Request $request)
    {
        $this->validate($request, [
            'name'=>"required|max:50",
            'email'=>"required|email",
            'phone'=>'required|max:20',
            'message'=>"required|max:200"
        ]);

        ContactMessage::create([
            "name"=>$request->get("name"),
            "email"=>$request->get("email"),
            "phone"=>$request->get("phone"),
            "message"=>$request->get("message")
        ]);

        Session::flash("success", "Your message was send successfully !. We will contact you soon");

        return redirect()->back();
    }

    public function getAbout()
    {
        $workers=CompanyLeader::orderBy("view_position", "asc")->get();
        return view("public.pages.about", compact("workers"));
    }
    public function getMission()
    {
        return view("public.pages.mission");
    }
    public function getVision()
    {
//        $workers=CompanyLeader::orderBy("view_position", "asc")->get();
        return view("public.pages.vission");
    }
    public function getObjective()
    {
        /*$workers=CompanyLeader::orderBy("view_position", "asc")->get();*/
        return view("public.pages.objective");
    }
    public function getStakeHolder()
    {
        $categories = BusinessCategory::all();
        $business=Business::select("id","title","description","business_category_id")->where("business_category_id",'=',2)->orderBy("id","asc")->get();

        return view("public.pages.stake_holder", compact("categories", "business"));
    }

    public function getGallery()
    {
      $gallery=ImageUpload::all();
        return view("public.gallery", compact("gallery"));
    }
    public function getTestimonial()
    {
        $testimonials=Feedback::select("id", "name", "position", "company", "image_url","saying")->where("status",1)->inRandomOrder()->take(4)->get();
        /*$testimonials=Feedback::all();*/

        return view("public.customer_feedback", compact("testimonials"));
    }
    public function getDealWithPatner()
    {
        $categories = BusinessCategory::all();
        $business=Business::select("id","title","description","business_category_id")->where("business_category_id",'=',1)->orderBy("id","asc")->get();
        return view("public.pages.deal_partner", compact("categories","business"));
    }
    public function getDealWithPatnerId($id)
    {
        $business=Business::findorfail($id);
        return view("public.pages.Mou_category", compact("business"));
    }
    public function getProductionUnit()
    {
        $production_unit=ProductionUnits::all();
        return view("public.pages.production_unit", compact("production_unit"));
    }
    public function getLatestActivities()
    {
        $activites=LatestActivities::all();
        return view("public.pages.latest_activity", compact("activites"));
    }
    public function getEvents()
    {
        $events=Event::all();
        return view("public.pages.upcoming_event", compact("events"));
    }

}
