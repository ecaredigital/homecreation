<?php

namespace App\Http\Controllers;

use App\Model\LatestActivities;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LatestActivitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activities=LatestActivities::all();
        return view("admin.latest_activities.index",compact("activities"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.latest_activities.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*dd($request);*/
        $this->validate($request,[
            "title"=>"required|max:255",
            "note"=>"required"
        ]);

        LatestActivities::create([
            "title"=>$request->get("title"),
            "description"=>$request->get("note")
        ]);

        Session::flash("success","Latest Activities added successfully");

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $activities=LatestActivities::findorfail($id);
        return view("admin.latest_activities.edit",compact("activities"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $activities=LatestActivities::findorfail($id);

        $this->validate($request,[
            "title"=>"required|max:255",
            "note"=>"required"
        ]);

        $activities->update([
            "title"=>$request->get("title"),
            "description"=>$request->get("note")
        ]);

        Session::flash("success","Latest Activities updated successfully");

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $activities=LatestActivities::findorfail($id);
        $activities->delete();
        Session::flash("success","Latest Activities has been deleted");
        return redirect()->back();
    }
}
