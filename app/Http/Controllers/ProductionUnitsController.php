<?php

namespace App\Http\Controllers;

use App\Model\ProductionUnits;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ProductionUnitsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $units=ProductionUnits::all();
        return view("admin.production_units.index",compact("units"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.production_units.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            "name" => "required|max:255",
            "address" => "required|max:255",
            "contact" => "required|max:13"
        ]);

        ProductionUnits::create([
            "name" => $request->get("name"),
            "address" =>  $request->get("address"),
            "contact" =>  $request->get("contact"),
        ]);


        Session::flash("success", "Production Units added successfully");

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $units=ProductionUnits::findorfail($id);
        return view("admin.production_units.edit",compact("units"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $units=ProductionUnits::findorfail($id);
        $this->validate($request, [
            "name" => "required|max:255",
            "address" => "required|max:255",
            "contact" => "required|max:13"
        ]);

         $units->update([
            "name" => $request->get("name"),
            "address" =>  $request->get("address"),
            "contact" =>  $request->get("contact"),
        ]);


        Session::flash("success", "Production Units added successfully");

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $units=ProductionUnits::findorfail($id);
        $units->delete();
        Session::flash("success","Production Unit has been deleted");
        return redirect()->back();
    }
}
