<?php

namespace App\Http\Controllers;

use App\Model\Business;
use App\Model\BusinessCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
class BusinessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = BusinessCategory::all();
        $business=Business::all();
        return view("admin.business.index", compact("categories", "business"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=BusinessCategory::all();
        return view("admin.business.create",compact("categories"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
          "title"=>"required|max:255",
           "note"=>"required"
            ]);

       Business::create([
           "title"=>$request->get("title"),
           "business_category_id"=>$request->get("category_id"),
           "description"=>$request->get("note"),
        ]);

        Session::flash("success","Business  added successfully");

          return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories=BusinessCategory::all();
        $business=Business::findorfail($id);
        return view("admin.business.edit",compact("categories","business"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $business=Business::findorfail($id);
        $this->validate($request,[
            "title"=>"required|max:255",
            "note"=>"required"
        ]);

        $business->update([
            "title"=>$request->get("title"),
            "business_category_id"=>$request->get("category_id"),
            "description"=>$request->get("note"),
        ]);

        Session::flash("success","Business updated successfully");

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categories=Business::findorfail($id);
        $categories->delete();
        Session::flash("success","Business Category has been deleted");
        return redirect()->back();
    }
}
