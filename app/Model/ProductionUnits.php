<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductionUnits extends Model
{
   protected $fillable=["name","address","contact"];
}
