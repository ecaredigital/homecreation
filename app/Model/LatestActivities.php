<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LatestActivities extends Model
{
    protected $fillable=["title","description"];
}
