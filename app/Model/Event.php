<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable=["title","start_date","end_date","start_time","end_time","venue","organized_by","details","image"];
}
