<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    protected $table = 'business';
    protected $fillable=["business_category_id","title","description"];

    public function category(){
        return $this->belongsTo(BusinessCategory::class);
    }
}